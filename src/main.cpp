#include <iostream>
#include <cmath>
#include <chrono>
#include <stdlib.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "resources.hpp"
#include "setup.hpp"
#include "move.hpp"

///TO DO: setup color for pixel only once, not every frame

extern const int windowHeight = 2160;
extern const int windowWidth = 1080;
const int vp = 11; //number of Vertical Points
const int hp = 6; //number of Horizontal Points
double vsq, hsq; 

extern const double randomness = 3; 
extern const double pointspeed = 15; //Seconds for 1 sine loop
extern const double colorspeed = 30; //Seconds for 1 colorloop

double ltInitColor = 175;
double rbInitColor = 230;
extern double colorRange = rbInitColor - ltInitColor;
colorset lt, rb;
int a_ch = 125; //the alpha channel for the triangles on the foreground

double totalpointtime = 0, totalcolortime = 0; 

double deltahue, deltasat, deltalum; 
extern double colorRange;

sf::ContextSettings settingstest;
sf::RenderWindow window;

sf::Image setImage(std::string img_path);
sf::Texture setTexture(std::string img_path);


int main()
{
	settingstest.antialiasingLevel = 8;
	sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "sfml_window", sf::Style::Default, settingstest);
	window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(60);

    vsq = (double)windowHeight / ((double)vp-1.0);
    hsq = (double)windowWidth / ((double)hp-1);
    std::chrono::high_resolution_clock clock;
    std::chrono::high_resolution_clock::time_point t0 , t1;
    std::chrono::duration<double> looptime;
    srand(time(NULL)); //create a new random seed
    sf::Event event;

	//set the background sprite. Useful when triangles on foreground are translucent and scaling etc. are required.
	std::string img_path = "src/image.png";
	sf::Texture bgTexture = setTexture(img_path);
	sf::Sprite bgSprite;

	bgSprite.setTexture(bgTexture);
	sf::Rect<float> size_bgSpr = bgSprite.getGlobalBounds();
	bgSprite.setScale(windowWidth / size_bgSpr.width, windowHeight / size_bgSpr.height);
	sf::Image bgImageSource = setImage(img_path); //Useful when pixeldata is requested or manipulated


    //Dynamically allocating memory for the pointer array
    //Array that holds points' Location, Original location, Offsets, LoopOffsets
    point** pointArray = new point*[vp];
    for (int i = 0; i < vp; i++)
		pointArray[i] = new point[hp];

    //Array that holds triangles' pointers to 3 points and the average of these locations
    triangle** triangleArray = new triangle*[vp-1];
    for(int i = 0; i < vp; i++)
        triangleArray[i] = new triangle[2*(hp-1)];

    lt.orhue = lt.hue = ltInitColor;
    rb.orhue = rb.hue = rbInitColor;

    lt.orsat = lt.sat = rb.orsat = rb.sat = 1.00; //0 to 100% saturation represented as a value of 0 to 1
    lt.orlum = lt.lum = rb.orlum = rb.lum = 0.50; //Luminance at 50% is average. 

    setInitialPointInfo(pointArray);
    linkTriangleArray(triangleArray, pointArray);
   // drawTriangles(triangleArray,&imageSource); //should this line of code be here? It gives some error with the drawTriangles_img function
    window.display();

    int loopnumber = 0;
    while (window.isOpen()) //START GAMELOOP
    {
        t0 = clock.now();
        while (window.pollEvent(event))
            if (event.type == sf::Event::Closed)
                window.close();
        
        movePoints(pointArray);
        window.clear(sf::Color::Black);
		window.draw(bgSprite); //draw background image. Useful when triangles on foreground are translucent.
        changeColor();
		
        //drawTriangles_grad(triangleArray, window);
		drawTriangles_img(triangleArray,window, &bgImageSource);
	
        window.display();

        t1 = clock.now();
        looptime = std::chrono::duration_cast<std::chrono::duration<double>>(t1 - t0);
		std::cout << "Time of 1 frame: " << looptime.count() <<" seconds"<<std::endl;
        totalpointtime = totalpointtime + looptime.count();
        totalcolortime = totalcolortime + looptime.count();
        if(totalpointtime > (pointspeed))
            totalpointtime = (double) ((int) (totalpointtime * 1000000) % 1000000) / 1000000;

        if(totalcolortime > (colorspeed))
            totalcolortime = (double) ((int) (totalcolortime * 1000000) % 1000000) / 1000000;
        loopnumber++;
        std::cout << loopnumber << std::endl;
        
        if(loopnumber == 100)
        {
            std::cout << "check" << std::endl;
            sf::Texture texture;
                texture.create(window.getSize().x, window.getSize().y);
                texture.update(window);
                if (texture.copyToImage().saveToFile("output.png"))
                {
                    std::cout << "screenshot saved to " << "output.png" << std::endl;
                }
        }
            
    }   //END GAMELOOPS

    //Clearing memory like a good boy
    for (int i = 0; i < vp; i++)
		delete[] pointArray[i];
	//delete[] pointArray; // thiis gives me an error if not commented out, but why should it not?

    for (int i = 0; i < vp-1; i++)
		delete[] triangleArray[i];
	//delete[] triangleArray;

    return 0;
}

///TO DO: move the functions below to anther file, but which one???
//function to set an image from a given path
sf::Image setImage(std::string img_path) {
	sf::Image img;
	//draw image MrH, from: http://www.cplusplus.com/forum/unices/109973/
	if (!img.loadFromFile(img_path))
	{
		std::cout << "Image NOT correctly loaded" << std::endl;
		system("pause");
		exit(1);
	}

	return img;
}

//gets the image on a given path and returns it as a sprite, so that it can be scaled and drawn onto the screen
sf::Texture setTexture(std::string img_path)
{
	//USEFUL link to prevent mistake from being made again: https://gamedev.stackexchange.com/questions/142330/sfml-sprite-not-being-rendered-when-returned-from-a-method
	sf::Image bgImageSource = setImage(img_path);
	sf::Texture bgTexture;

	if (!bgTexture.loadFromImage(bgImageSource))
	{
		std::cout << "Texture NOT correctly loaded" << std::endl;
		system("pause");
		exit(1);
	}

	return bgTexture;
}