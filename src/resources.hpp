#pragma once
#include <SFML/Graphics.hpp>

extern const int windowHeight;
extern const int windowWidth;
extern const int vp;
extern const int hp;
extern double vsq, hsq;

extern const double randomness;
extern const double pointspeed;
extern const double colorspeed;
extern double totalpointtime, totalcolortime;
extern int loopnumber;

struct colorset
    {
        double orhue, orsat, orlum, hue, sat, lum, direction = 1;
    };

extern colorset lt, rb;
extern double deltahue, deltasat, deltalum; 
extern double colorRange;

extern sf::RenderWindow window;

struct point
    {double x, y, orx, ory, offx, offy, loopoffx, loopoffy;};

struct triangle
{
    double *x1, *y1, *x2, *y2, *x3, *y3, avgx , avgy;
    int rgb[3];
    int hueOffset;
};

void wait(double sleeptimeMS);
void getColor(int *rgb, double x, double y, int hueOffset);
void RGBToHSL(int R, int G, int B, double& H, double& S, double& L);
void HSLtoRGB(double hue, double sat, double lum, int *rgb);