#include "setup.hpp"
#include <cmath>
#include <iostream>
#include <stdlib.h>

void setInitialPointInfo(point **pointArray)      
{
    for(int i = 0; i < (vp); i++)
    {
        for(int j = 0; j < (hp); j++)
        {
            pointArray[i][j].orx = j * hsq; //Setting original location (grid without offset)
            pointArray[i][j].ory = i * vsq;
            pointArray[i][j].offx = (((double) (rand() % 2001)) / 1000) - 1; //Setting offset value = random value between 0 and 1
            pointArray[i][j].offy = (((double) (rand() % 1001)) / 1000) - 1;

            if(j == 0 || j == hp-1)
                pointArray[i][j].offx = 0; //Setting x and y offsets to 0 of points along the border of the window
            if(i == 0 || i == vp-1)
                pointArray[i][j].offy = 0;

            pointArray[i][j].loopoffx = (double) (rand() % 1000) / 1000; //Random offset in the sineloop 
            pointArray[i][j].loopoffy = (double) (rand() % 1000) / 1000;
        }
    }

}

void linkTriangleArray(triangle** triangleArray, point **pointArray)
{
    int randInt;
    for(int i = 0; i < (vp-1); i++)
    {
        for(int j = 0; j < (hp-1); j++)
        {
            //This sets the 3 corner points that make up a triangle for every triangle.
            randInt = rand() % 2; //random number either 0 or 1
            if(randInt > 0)
            {
                triangleArray[i][2*j].x1 = &pointArray[i][j].x;
                triangleArray[i][2*j].y1 = &pointArray[i][j].y;
                triangleArray[i][2*j+1].x1 = &pointArray[i+1][j+1].x;
                triangleArray[i][2*j+1].y1 = &pointArray[i+1][j+1].y;

                triangleArray[i][2*j].x2 = triangleArray[i][2*j+1].x2 = &pointArray[i][j+1].x;
                triangleArray[i][2*j].y2 = triangleArray[i][2*j+1].y2 = &pointArray[i][j+1].y;
                triangleArray[i][2*j].x3 = triangleArray[i][2*j+1].x3 = &pointArray[i+1][j].x;
                triangleArray[i][2*j].y3 = triangleArray[i][2*j+1].y3 = &pointArray[i+1][j].y;
            } 
            else
            {
                triangleArray[i][2*j].x1 = &pointArray[i][j+1].x;
                triangleArray[i][2*j].y1 = &pointArray[i][j+1].y;
                triangleArray[i][2*j+1].x1 = &pointArray[i+1][j].x;
                triangleArray[i][2*j+1].y1 = &pointArray[i+1][j].y;

                triangleArray[i][2*j].x2 = triangleArray[i][2*j+1].x2 = &pointArray[i][j].x;
                triangleArray[i][2*j].y2 = triangleArray[i][2*j+1].y2 = &pointArray[i][j].y;
                triangleArray[i][2*j].x3 = triangleArray[i][2*j+1].x3 = &pointArray[i+1][j+1].x;
                triangleArray[i][2*j].y3 = triangleArray[i][2*j+1].y3 = &pointArray[i+1][j+1].y;
            }   
            //int randHueOffset = rand() % 60 - 30;
            triangleArray[i][2*j].hueOffset = rand() % 120 - 60;
            triangleArray[i][2*j+1].hueOffset = rand() % 120 - 60;
        }
    }
}
