#include "move.hpp"
#include <cmath>
#include <iostream>
#include <SFML/System/Vector2.hpp>
#define pi 3.14159265358979

extern int a_ch;

void movePoints(point **pointArray) 
{
    double moveFactor; 
    for(int i = 0; i < vp; i++)
    {
        for(int j = 0; j < hp; j++)
        {   
            //x = origx + randomness * offsetx * 100 * (sin(totalTime) + 1) + offsetx;
            pointArray[i][j].x = pointArray[i][j].orx + (randomness / 10) * pointArray[i][j].offx * hsq * (sin(2*pi*((totalpointtime/pointspeed) + pointArray[i][j].loopoffx)) + 1) + pointArray[i][j].offx; 
            pointArray[i][j].y = pointArray[i][j].ory + (randomness / 10) * pointArray[i][j].offy * vsq * (sin(2*pi*((totalpointtime/pointspeed) + pointArray[i][j].loopoffy)) + 1) + pointArray[i][j].offy;
        }
    }
}


void drawTriangles_grad(triangle **triangleArray, sf::RenderWindow &(window))
{
	sf::ConvexShape convex;
	convex.setPointCount(3);
	for (int i = 0; i < (vp - 1); i++)
	{
		for (int j = 0; j < (2 * (hp - 1)); j++)
		{
			triangleArray[i][j].avgx = (*(triangleArray[i][j].x1) + *(triangleArray[i][j].x2) + *(triangleArray[i][j].x3)) / 3;
			triangleArray[i][j].avgy = (*(triangleArray[i][j].y1) + *(triangleArray[i][j].y2) + *(triangleArray[i][j].y3)) / 3;

			getColor(triangleArray[i][j].rgb, triangleArray[i][j].avgx, triangleArray[i][j].avgy, triangleArray[i][j].avgx);

			convex.setPoint(0, sf::Vector2f((float)*(triangleArray[i][j].x1), (float)*(triangleArray[i][j].y1)));
			convex.setPoint(1, sf::Vector2f((float)*(triangleArray[i][j].x2), (float)*(triangleArray[i][j].y2)));
			convex.setPoint(2, sf::Vector2f((float)*(triangleArray[i][j].x3), (float)*(triangleArray[i][j].y3)));
			convex.setOutlineThickness(0);
			//convex.setOutlineColor(sf::Color(std::abs(255 - triangleArray[i][j].rgb[0]), std::abs(255 - triangleArray[i][j].rgb[1]), std::abs(255 - triangleArray[i][j].rgb[2]), 100));
			convex.setOutlineColor(sf::Color(0,0,0));
			convex.setFillColor(sf::Color(triangleArray[i][j].rgb[0], triangleArray[i][j].rgb[1], triangleArray[i][j].rgb[2],a_ch));
			window.draw(convex);
		}
	}
}


int isFirstRun = true;
void drawTriangles_img(triangle **triangleArray, sf::RenderWindow &(window), sf::Image* img)
{
	sf::ConvexShape convex;
	convex.setPointCount(3);
	for (int i = 0; i < (vp - 1); i++)
	{
		for (int j = 0; j < (2 * (hp - 1)); j++)
		{
			triangleArray[i][j].avgx = (*(triangleArray[i][j].x1) + *(triangleArray[i][j].x2) + *(triangleArray[i][j].x3)) / 3;
			triangleArray[i][j].avgy = (*(triangleArray[i][j].y1) + *(triangleArray[i][j].y2) + *(triangleArray[i][j].y3)) / 3;

			convex.setPoint(0, sf::Vector2f((float)*(triangleArray[i][j].x1), (float)*(triangleArray[i][j].y1)));
			convex.setPoint(1, sf::Vector2f((float)*(triangleArray[i][j].x2), (float)*(triangleArray[i][j].y2)));
			convex.setPoint(2, sf::Vector2f((float)*(triangleArray[i][j].x3), (float)*(triangleArray[i][j].y3)));
			convex.setOutlineThickness(0);
			convex.setOutlineColor(sf::Color(0,0,0,50));

			sf::Color pixCol = sf::Color::Magenta;
			//in the first run fetch the color each triangle should have from the image and save this color into the triangles properties
			if (isFirstRun)
			{
				sf::Vector2u imgSize = img->getSize();
				int pix[] = { triangleArray[i][j].avgx * (imgSize.x - 1) / windowWidth, triangleArray[i][j].avgy *(imgSize.y - 1) / windowHeight }; //use pixel color at average position of triangle

				if (pix[0] <= imgSize.x && pix[1] <= imgSize.y)
				{
					int rectSide = 0; //should be >=0
					unsigned int rectSteps = 1; //the steps with which the rectangle is scanned; MUST BE >=1
					//to prevent an infinite loop: if rectsteps is too small, correct its value
					if (rectSteps <= 0)
					{
						rectSteps = 1;
						std::cout << "value of RectSteps too small! Value is reset to 1\n";
					}
					//to prevent an unwanted behaviour: if rectSide is too small, correct its value
					if (rectSide < 0)
					{
						rectSide = 1;
						std::cout << "value of RectSide too small! Value is reset to 1\n";
					}
					for (int i = pix[1] - rectSide / 2; i <= pix[1] + rectSide / 2; i += rectSteps)
					{
						pixCol = img->getPixel(pix[0], pix[1]);//since the average of pixcol and the color of some pixel will be taken, set pixcol to the color of the pixel in the centre of the averaging box aka the original pixel
						for (int j = pix[0] - rectSide / 2; j <= pix[0] + rectSide / 2; j += rectSteps)
						{
							int p[] = { (pixCol.r + img->getPixel(pix[0], pix[1]).r) / 2, (pixCol.g + img->getPixel(pix[0], pix[1]).g) / 2 ,(pixCol.b + img->getPixel(pix[0], pix[1]).b) / 2 };
							pixCol = sf::Color(p[0], p[1], p[2]);
						}
					}
				}
				int redval = pixCol.r;
				int blueval = pixCol.b;
				int greenval = pixCol.g;

				int offsetValue = 60;
				redval = redval + rand() % offsetValue - offsetValue / 2;
				//greenval = greenval + rand() % offsetValue - offsetValue / 2;
				//blueval = blueval + rand() % offsetValue - offsetValue / 2;
				while(redval > 255 || greenval > 255 || blueval > 255 || redval < 0 || greenval < 0 || blueval < 0 )
				{
					if(redval > 255)
					{
						greenval = greenval + redval - 255;
						blueval = blueval - redval + 255;
						redval = 255;
					}
					else if(greenval > 255)
					{
						blueval = blueval + greenval - 255;
						redval = redval - greenval + 255;
						greenval = 255;
					}
					else if(blueval > 255)
					{
						redval = redval + blueval - 255;
						greenval = greenval - blueval + 255;
						blueval = 255;
					}

					else if(redval < 0)
					{
						blueval = blueval - redval;
						greenval = greenval + redval;
						redval = 0;
					}
					else if(greenval < 0)
					{
						redval = redval - greenval;
						blueval = blueval + greenval;
						greenval = 0;
					}
					else if(blueval < 0)
					{
						greenval = greenval - blueval;
						redval = redval + blueval;
						blueval = 0;
					}
				}
				std::cout << "filcolor set " << i << " " << j << std::endl;

				triangleArray[i][j].rgb[0] = redval;
				triangleArray[i][j].rgb[1] = greenval;
				triangleArray[i][j].rgb[2] = blueval;
				//pixCol.a = a_ch;
				convex.setFillColor(sf::Color(triangleArray[i][j].rgb[0], triangleArray[i][j].rgb[1], triangleArray[i][j].rgb[2]));
			}
			//in all runs except the first fetch the color of each triangle from its propterties
			else
			{
				convex.setFillColor(sf::Color(triangleArray[i][j].rgb[0], triangleArray[i][j].rgb[1], triangleArray[i][j].rgb[2], a_ch));
			}
			window.draw(convex);
		}	
	}
	isFirstRun = false; //set to true here if you want to update the color of all triangles every frame depending on its (new) position
}

void changeColor()
{
    lt.hue = lt.orhue + std::fmod(360*(totalcolortime/colorspeed),360);
    rb.hue = lt.hue + colorRange;
}
