#include <cmath>
#include "resources.hpp"

//Extra includes are just for debugging using the sleep function
#ifdef __WIN32
#include <windows.h>
#endif


void wait(double sleeptime)
{
    #ifdef __WIN32
        Sleep(sleeptime);
    #endif
}

void getColor(int *rgb, double x, double y, int hueOffset)
{
    double C, X, m;
    double hue, sat, lum, tlum;
    
    hue = std::fmod(lt.hue + (colorRange / 3) *  ((2*x) / windowWidth + (1*y) / windowHeight) + hueOffset,360);

    
    sat = 1;
    //abs(lt.sat + (((rb.sat-lt.sat)/2) * (x / windowWidth + y / windowHeight)));
    lum = 0.5;

    //abs(lt.lum + (((rb.lum-lt.lum)/2) * (x / windowWidth + y / windowHeight)));

    C = (1 - std::abs(2 * lum - 1)) * sat;
    X = C * (1 - std::abs(std::fmod((hue/60),2) - 1)); 
    m = lum - (C / 2);

    if(hue > 0 && hue <= 60)
    {
        *rgb = (C + m) * 255;
        *(rgb+1) = (X + m) * 255;
        *(rgb+2) = 0;
    }
    else if(hue > 60 && hue <= 120)
    {
        *rgb = (X + m) * 255;
        *(rgb+1) = (C + m) * 255;
        *(rgb+2) = 0;
    }
    else if(hue > 120 && hue <= 180)
    {
        *rgb = 0;
        *(rgb+1) = (C + m) * 255;
        *(rgb+2) = (X + m) * 255;
    }
    else if(hue > 180 && hue <= 240)
    {
        *rgb = 0;
        *(rgb+1) = (X + m) * 255;
        *(rgb+2) = (C + m) * 255;
    }
    else if(hue > 240 && hue <=300)
    {
        *rgb = (X + m) * 255;
        *(rgb+1) = 0;
        *(rgb+2) = (C + m) * 255;
    }
    else
    {
        *rgb = (C + m) * 255;
        *(rgb+1) = 0;
        *(rgb+2) = (X + m) * 255;
    }
}

#include <iostream>
#include <cstdlib>
#include <cmath>
float Min(float fR, float fG, float fB)
{
	float fMin = fR;
	if (fG < fMin)
	{
		fMin = fG;
	}
	if (fB < fMin)
	{
		fMin = fB;
	}
	return fMin;
}


float Max(float fR, float fG, float fB)
{
	float fMax = fR;
	if (fG > fMax)
	{
		fMax = fG;
	}
	if (fB > fMax)
	{
		fMax = fB;
	}
	return fMax;
}

void RGBToHSL(int R, int G, int B, double& H, double& S, double& L)
{
	int r = 100.0;
	int g = 100.0;
	int b = 200.0;

	float fR = r / 255.0;
	float fG = g / 255.0;
	float fB = b / 255.0;


	float fCMin = Min(fR, fG, fB);
	float fCMax = Max(fR, fG, fB);


	L = 50 * (fCMin + fCMax);

	if (fCMin = fCMax)
	{
		S = 0;
		H = 0;
		return;

	}
	else if (L < 50)
	{
		S = 100 * (fCMax - fCMin) / (fCMax + fCMin);
	}
	else
	{
		S = 100 * (fCMax - fCMin) / (2.0 - fCMax - fCMin);
	}

	if (fCMax == fR)
	{
		H = 60 * (fG - fB) / (fCMax - fCMin);
	}
	if (fCMax == fG)
	{
		H = 60 * (fB - fR) / (fCMax - fCMin) + 120;
	}
	if (fCMax == fB)
	{
		H = 60 * (fR - fG) / (fCMax - fCMin) + 240;
	}
	if (H < 0)
	{
		H = H + 360;
	}
}

void HSLtoRGB(double hue, double sat, double lum, int *rgb)
{

	double C, X, m;
    double tlum;
	//abs(lt.sat + (((rb.sat-lt.sat)/2) * (x / windowWidth + y / windowHeight)));

	hue = 120;
    //abs(lt.lum + (((rb.lum-lt.lum)/2) * (x / windowWidth + y / windowHeight)));

    C = (1 - std::abs(2 * lum - 1)) * sat;
    X = C * (1 - std::abs(std::fmod((hue/60),2) - 1)); 
    m = lum - (C / 2);

    if(hue > 0 && hue <= 60)
    {
        *rgb = (C + m) * 255;
        *(rgb+1) = (X + m) * 255;
        *(rgb+2) = 0;
    }
    else if(hue > 60 && hue <= 120)
    {
        *rgb = (X + m) * 255;
        *(rgb+1) = (C + m) * 255;
        *(rgb+2) = 0;
    }
    else if(hue > 120 && hue <= 180)
    {
        *rgb = 0;
        *(rgb+1) = (C + m) * 255;
        *(rgb+2) = (X + m) * 255;
    }
    else if(hue > 180 && hue <= 240)
    {
        *rgb = 0;
        *(rgb+1) = (X + m) * 255;
        *(rgb+2) = (C + m) * 255;
    }
    else if(hue > 240 && hue <=300)
    {
        *rgb = (X + m) * 255;
        *(rgb+1) = 0;
        *(rgb+2) = (C + m) * 255;
    }
    else
    {
        *rgb = (C + m) * 255;
        *(rgb+1) = 0;
        *(rgb+2) = (X + m) * 255;
    }
}